pipelineJob('pipelineJob') {
    definition {
        cps {
            script(readFileFromWorkspace('pipelineJob.groovy'))
            sandbox()
        }
    }
}
pipelineJob('deal-refresh-job') {
    definition {
        cpsScm {
            scm {
                git {
                    remote {
                        url 'https://anthonyquinn@bitbucket.org/wowcher/deal-refresh.git'
                    }
                    branch 'master'
                }
            }
        }
    }
}